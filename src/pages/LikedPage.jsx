import React from 'react';
import CardInfo from '../components/CardInfo';
import StarIcon from '../components/StarIcon';

export default function LikedPage(props) {
    return (
        <div className="cards">
            {JSON.parse(localStorage.getItem('likedItems'))?.map((product) => (
                <CardInfo 
                    product={product} 
                    class="card"
                    key={product.articul}
                    button={
                        <StarIcon 
                            fillLiked={"yellow"}
                            product={product}
                            remove="true"
                        />
                    }
                />
            ))}
        </div>
    );
}