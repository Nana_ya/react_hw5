import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Button from 'react-bootstrap/Button';
import ButtonModal from './ButtonModal';
import ModalWindow from './ModalWindow';
import CardInfo from "./CardInfo";
import { useFunction } from '../hook/useFunction';
import { useSelector, useDispatch } from 'react-redux';
import { MODAL_WINDOW_SHOW_TRUE, 
    MODAL_WINDOW_SHOW_FALSE,
    GET_PRODUCTS_FROM_JSON } from "../redux/actions";

    const productsThunk = () => {
        return (dispatch, getState) => {
            dispatch({type: 'GET_PRODUCTS_FROM_JSON_REQUEST'});
            fetch('./products.json')
                .then(response => response.json())
                .then(data => {
                    // console.log("data", data.products);
                    dispatch({type: GET_PRODUCTS_FROM_JSON, payload: {products: data.products}});
                    
                })
                .catch(error => console.error(error)); 
          
        }
      }

export default function Cards() {
    
    const [lastChosenProduct, setLastChosenProduct] = useState({});
    const {basketUser} = useFunction();
    const {likedItems} = useFunction();
    const {unlikedProduct} = useFunction();
    const {likedProduct} = useFunction();
    const products = useSelector(state => state.products.products);
    const basket = useSelector(state => state.basket.cart);
    const show = useSelector(state => state.show);
    const isLoading = useSelector(state => state.products.isLoading);
    const dispatch = useDispatch();
    // console.log("cards", basket);

    useEffect(() => {
        dispatch(productsThunk());
    }, [dispatch])

    let firstModalButtons = 
        <>
            <Button className='modal-button-left' onClick={ () => {
                basketUser(lastChosenProduct);
                dispatch({type: MODAL_WINDOW_SHOW_FALSE});
            }}>
                Ok
            </Button>
            <Button className='modal-button-right' onClick={() => {dispatch({type: MODAL_WINDOW_SHOW_FALSE});}}>
                No
            </Button>
        </>


        const textFirstModal = "Are you sure you want to add this product to the cart?";
        const headerFirstModal = "Do you want to delete this file?";
        // console.log(products);
        return (
            <div>
                <ModalWindow
                header={headerFirstModal}
                text={textFirstModal}
                closeButton={true}
                color="red"
                actions={firstModalButtons}
                handleShow={() => {dispatch({type: MODAL_WINDOW_SHOW_TRUE});}}
                handleClose={() => {dispatch({type: MODAL_WINDOW_SHOW_FALSE});}}
                show1={show}
                lastChosenProduct={lastChosenProduct}
                />
            
                <div className="cards">
                    {isLoading && <h1>Loading...</h1>}
                    {products.map((product) => (
                        <div className="cards" key={product.articul}>
                            <CardInfo 
                            product={product}
                            likedItems={likedItems}
                            likedProduct={likedProduct}
                            unlikedProduct={unlikedProduct}
                            class="card"
                            button={
                                <ButtonModal
                                    backgroundColor={
                                        {
                                            backgroundColor: "red",
                                            border: 0,
                                        }
                                    }
                                    text={"Add to the cart"}
                                    onClick={()=>{
                                        dispatch({type: MODAL_WINDOW_SHOW_TRUE});
                                        setLastChosenProduct(product);
                                    }}
                                ></ButtonModal >
                            } 
                        />
                
                        </div>
                    ))}
                </div>
    </div>

    );
}

Cards.propTypes = {
    basket: PropTypes.func,
    likedItems: PropTypes.array,
    likedProduct: PropTypes.func,
    unlikedProduct: PropTypes.func
}